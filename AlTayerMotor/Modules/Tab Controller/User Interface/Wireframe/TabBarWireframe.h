//
//  TabbarWireframe.h
//  AlTayerMotor
//
//  Created by Niteco Macmini 5wdwyl  on 10/21/15.
//  Copyright © 2015 Niteco. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FirstTimePresenter;

@interface TabBarWireframe : NSObject

- (void)presentTabbarInterfaceFromWindow:(UIWindow *)window;

@end

