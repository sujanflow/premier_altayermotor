//
//  ServicesViewInterface.h
//  AlTayerMotors
//
//  Created by Niteco Macmini 5wdwyl  on 11/5/15.
//  Copyright © 2015 Niteco. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ServicesViewInterface <NSObject>
- (void)updateRegisteredVehicles:(NSArray *)vehicles;
@end
