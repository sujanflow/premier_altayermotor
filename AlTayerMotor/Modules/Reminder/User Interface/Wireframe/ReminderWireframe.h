//
//  ReminderWireframe.h
//  AlTayerMotor
//
//  Created by Niteco Macmini 5wdwyl  on 10/23/15.
//  Copyright © 2015 Niteco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReminderWireframe : NSObject

- (void)presentReminderInParentViewController:(UIViewController*)viewController withDate:(NSString *)dateStr;

@end
