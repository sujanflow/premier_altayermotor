//
//  User+CoreDataProperties.m
//  AlTayerMotors
//
//  Created by Niteco Macmini 5wdwyl  on 3/1/16.
//  Copyright © 2016 Niteco. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

@dynamic city;
@dynamic first_name;
@dynamic id;
@dynamic last_name;
@dynamic phone_code;
@dynamic phone_number;

@end
