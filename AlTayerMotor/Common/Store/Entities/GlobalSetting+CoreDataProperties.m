//
//  GlobalSetting+CoreDataProperties.m
//  AlTayerMotors
//
//  Created by Niteco Macmini 5wdwyl  on 3/1/16.
//  Copyright © 2016 Niteco. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "GlobalSetting+CoreDataProperties.h"

@implementation GlobalSetting (CoreDataProperties)

@dynamic index;
@dynamic key;
@dynamic name;
@dynamic nameAR;
@dynamic parent_key;

@end
