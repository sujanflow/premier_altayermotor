//
//  WelcomeCell.h
//  AlTayerMotors
//
//  Created by Niteco Macmini 5wdwyl  on 2/1/16.
//  Copyright © 2016 Niteco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;

@end
